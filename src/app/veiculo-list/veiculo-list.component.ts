import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-veiculo-list',
  templateUrl: './veiculo-list.component.html',
  styleUrls: ['./veiculo-list.component.css']
})
export class VeiculoListComponent implements OnInit {

  veiculos;
  veiculoSelecionado;

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.dataService.get().subscribe(resultado => {this.veiculos = resultado});
  }

  selectVeiculo(veiculo){
    this.veiculoSelecionado = veiculo;
  }

  deleteVeiculo(id){
    this.dataService.delete(id).subscribe(r => {this.dataService.get().subscribe(resultado => {this.veiculos = resultado});});
  }
  
}
