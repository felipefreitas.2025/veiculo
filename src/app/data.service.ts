import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  veiculos = [];

  constructor(private http: HttpClient) { }

  public get():Observable<any>{
    return this.http.get("http://localhost:8080/veiculos");
  }

  public post(veiculo: {id, placa, modelo, anoFabricacao}) : Observable<any>{
    return this.http.post("http://localhost:8080/veiculos", veiculo);
  }

  public delete(id):Observable<any>{
    return this.http.delete(`http://localhost:8080/veiculos/${id}`);
  }

}
