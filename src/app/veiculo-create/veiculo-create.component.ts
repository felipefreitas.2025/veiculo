import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-veiculo-create',
  templateUrl: './veiculo-create.component.html',
  styleUrls: ['./veiculo-create.component.css']
})
export class VeiculoCreateComponent implements OnInit {

  veiculo: { id, placa, modelo, anoFabricacao } = { id: null, placa: "", modelo: "", anoFabricacao: null };

  constructor(public dataService: DataService) { }

  ngOnInit(): void {
  }

  createVeiculo() {
    console.log(this.veiculo);
    this.dataService.post(this.veiculo).subscribe(resultado => {
      this.veiculo = { id: null, placa: "", modelo: "", anoFabricacao: null };
    });
  }

}
